import express from "express";
import config from "config";
import mongoose from "mongoose";
import { authRoutes } from "./routes/auth/auth.routes.js";
import { todoRouter } from "./routes/todos/todos.routes.js";
import { userRouter } from "./routes/user/user.routes.js";
import cors from "cors";

const app = express();

const PORT = config.get("port") || 5000;
const MONGO_DB_URL = config.get("mongoDbUrl");

app.use(
  express.json({
    extended: true,
  })
);
app.use(cors());

app.use("/api/auth", authRoutes);
app.use("/api/user", userRouter);
app.use("/api/todo", todoRouter);

async function start() {
  try {
    await mongoose.connect(MONGO_DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    app.listen(PORT, () => {
      console.log("Server start on 5000 port");
    });
  } catch (e) {
    console.log(e);
  }
}
start();
